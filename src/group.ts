import {getPart, ScrambleParts} from './part';

export type ScrambleGroup = Array<ScrambleParts>;

export function getTilesGroups(
  width: number,
  height: number,
  tile: number,
): ScrambleGroup {
  const totalParts = Math.ceil(width / tile) * Math.ceil(height / tile);
  const g: {[key: string]: ScrambleParts} = {};
  for (let i = 0; i < totalParts; i++) {
    const s = getPart(i, width, height, tile);
    const key = `${s.size.w}-${s.size.h}`;
    if (!g[key]) g[key] = [];
    g[key].push(s);
  }
  return Object.values(g);
}

function getColsInGroup(tiles: ScrambleParts): number {
  if (tiles.length == 1) return 1;
  const t = tiles[0].pos.y;
  const i = tiles.findIndex((s) => t != s.pos.y);
  return i == -1 ? tiles.length : i;
}

export interface ScrambleGroupInfo {
  tiles: number;
  cols: number;
  rows: number;
  width: number;
  height: number;
  x: number;
  y: number;
}

export function getTilesGroupInfo(tiles: ScrambleParts): ScrambleGroupInfo {
  const {length} = tiles;
  const {pos, size} = tiles[0];
  const {x, y} = pos;
  const {w, h} = size;
  const cols = getColsInGroup(tiles);
  const rows = length / cols;
  const width = w * cols;
  const height = h * rows;
  return {tiles: length, cols, rows, width, height, x, y};
}
