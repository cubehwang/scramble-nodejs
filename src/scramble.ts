import Jimp from 'jimp';
import {ScrambleImage, ScrambleOptions} from '.';
import {getTilesGroupInfo, getTilesGroups} from './group';
import {getPartPos} from './part';
import {generateShuffleIndex} from './shuffle';

export class Scrambler {
  constructor(private raw: ScrambleImage, private options: ScrambleOptions) {}

  private async shake(img: Jimp) {
    const {tile} = this.options;
    const {width, height} = this.raw.bitmap;
    getTilesGroups(width, height, tile).forEach((tiles) => {
      const shuffleInd = generateShuffleIndex(tiles.length, this.options.seed);
      const group = getTilesGroupInfo(tiles);
      tiles.forEach((t, i) => {
        const {w, h} = t.size;
        const pos = getPartPos(shuffleInd[i], group.width, tile);
        const srcPos = getPartPos(i, group.width, tile);
        img.blit(
          this.raw,
          group.x + pos.x,
          group.y + pos.y,
          group.x + srcPos.x,
          group.y + srcPos.y,
          w,
          h,
        );
      });
    });
  }

  async export(filepath: string) {
    const {width, height} = this.raw.bitmap;
    const img = await Jimp.create(width, height);
    await this.shake(img);
    await img.writeAsync(filepath);
  }

  async buffer(mimeType: string): Promise<Buffer> {
    const {width, height} = this.raw.bitmap;
    const img = await Jimp.create(width, height);
    await this.shake(img);
    return await img.getBufferAsync(mimeType);
  }
}
