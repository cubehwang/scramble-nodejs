import sharp from 'sharp';
import axios from 'axios';
import {ScrambleOptions} from '.';
import {getTilesGroupInfo, getTilesGroups} from './group';
import {getPartPos} from './part';
import {generateShuffleIndex} from './shuffle';
import {trackMemory} from './track';

export class Scrambler {
  constructor(private options: ScrambleOptions) {}

  private raw?: sharp.Sharp;
  private width = 0;
  private height = 0;

  async loadUrl(url: string) {
    const {data} = await axios(url, {responseType: 'arraybuffer'});
    this.raw = sharp(Buffer.from(data, 'binary')).raw();
    const meta = await this.raw?.metadata();
    this.width = meta.width || 0;
    this.height = meta.height || 0;
  }

  private async shake(img: sharp.Sharp) {
    if (!this.raw) return;
    const {raw, width, height} = this;
    const {tile} = this.options;
    trackMemory('shake');
    const buffs: Array<any> = getTilesGroups(width, height, tile)
      .map((tiles) => {
        const shuffleInd = generateShuffleIndex(
          tiles.length,
          this.options.seed,
        );
        const group = getTilesGroupInfo(tiles);
        return tiles.map((t, i) => {
          const {w, h} = t.size;
          const pos = getPartPos(shuffleInd[i], group.width, tile);
          const srcPos = getPartPos(i, group.width, tile);
          return {
            input: raw
              .extract({
                top: group.y + srcPos.y,
                left: group.x + srcPos.x,
                width: w,
                height: h,
              })
              .png()
              .toFile(`output/${group.y + srcPos.y}_${group.x + srcPos.x}.png`),
            left: group.x + pos.x,
            top: group.y + pos.y,
          };
        });
      })
      .flat();
    trackMemory('shake');
    await Promise.all(
      buffs.map(async (buff) => {
        buff.input = await buff.input;
        trackMemory('shake');
      }),
    );
    trackMemory('shake');
    img.composite(buffs.map(({input, top, left}) => ({input, top, left})));
  }

  async export(filepath: string) {
    if (!this.raw) return;
    trackMemory('export');
    const img = this.raw.clone();
    trackMemory('export');
    await this.shake(img);
    trackMemory('export');
    await img.toFile(filepath);
    trackMemory('export');
  }

  async buffer(): Promise<Buffer | null> {
    if (!this.raw) return null;
    trackMemory('buffer');
    const img = this.raw.clone();
    trackMemory('buffer');
    await this.shake(img);
    trackMemory('buffer');
    const buff = await img.toBuffer();
    trackMemory('buffer');
    return buff;
  }
}
