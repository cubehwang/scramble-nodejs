let init = true;

export function trackMemory(tag: string) {
  if (init) {
    init = false;
    console.log(
      [
        'Tag'.padEnd(12),
        'rss'.padStart(12),
        'heapTotal'.padStart(12),
        'heapUsed'.padStart(12),
        'external'.padStart(12),
        'buffers'.padStart(12),
      ].join(''),
    );
  }

  const {rss, heapTotal, heapUsed, external, arrayBuffers} =
    process.memoryUsage();
  console.log(
    [
      tag.padEnd(12),
      formatNumber(rss),
      formatNumber(heapTotal),
      formatNumber(heapUsed),
      formatNumber(external),
      formatNumber(arrayBuffers),
    ].join(''),
  );
}

function formatNumber(n: number) {
  return (n / 1024 / 1024).toFixed(1).padStart(12);
}
