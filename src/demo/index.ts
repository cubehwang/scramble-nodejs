import {Scrambler} from '../scramble-sharp';
import {trackMemory} from '../track';

(async function () {
  trackMemory('main');
  const scrambler = new Scrambler({
    seed: 'CubeHwang',
    tile: 50,
  });
  trackMemory('main');
  await scrambler.loadUrl('http://localhost:55072/sample.png');
  trackMemory('main');
  await scrambler.export('./output/sample.png');
  trackMemory('main');
})();
