import shuffleSeed from 'shuffle-seed';

export function generateShuffleIndex(size: number, seed: string) {
  return shuffleSeed.shuffle([...Array(size).keys()], seed);
}
