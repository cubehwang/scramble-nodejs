import axios from 'axios';
import Jimp from 'jimp';
import {ScrambleImage} from '.';

export function loadFile(filepath: string): Promise<ScrambleImage> {
  return Jimp.read(filepath);
}

export function loadUrl(url: string): Promise<ScrambleImage> {
  return axios(url, {responseType: 'arraybuffer'})
    .then(({data}) => Buffer.from(data, 'binary'))
    .then((buff) => Jimp.read(buff));
}

// export function loadBuffer(buff: Buffer) {
//   //TODO
// }
