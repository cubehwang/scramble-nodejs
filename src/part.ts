export interface ScramblePosition {
  x: number;
  y: number;
}

export interface ScrambleSize {
  w: number;
  h: number;
}

export interface ScramblePart {
  pos: ScramblePosition;
  size: ScrambleSize;
}

export type ScrambleParts = Array<ScramblePart>;

export function getPartPos(
  part: number,
  width: number,
  tile: number,
): ScramblePosition {
  const verticalSlices = Math.ceil(width / tile);
  const row = (part / verticalSlices) | 0;
  const col = part - row * verticalSlices;
  const x = Math.floor(col * tile);
  const y = Math.floor(row * tile);
  return {x, y};
}

export function getPart(
  part: number,
  width: number,
  height: number,
  tile: number,
): ScramblePart {
  const pos = getPartPos(part, width, tile);
  const w = tile - Math.max(0, pos.x + tile - width);
  const h = tile - Math.max(0, pos.y + tile - height);
  const size = {w, h};
  return {pos, size};
}
