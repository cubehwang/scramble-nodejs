export interface ScrambleOptions {
  seed: string;
  tile: number;
}
