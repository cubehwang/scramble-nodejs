import axios from 'axios';
import {writeFileSync} from 'fs';
import {createCanvas, loadImage, Image, Canvas} from 'canvas';
import {ScrambleOptions} from '.';
import {getTilesGroupInfo, getTilesGroups} from './group';
import {getPartPos} from './part';
import {generateShuffleIndex} from './shuffle';
import {trackMemory} from './track';

export class Scrambler {
  constructor(private options: ScrambleOptions) {}

  private raw?: Image;

  async loadUrl(url: string) {
    const {data} = await axios(url, {responseType: 'arraybuffer'});
    this.raw = await loadImage(Buffer.from(data, 'binary'));
  }

  private async shake(canvas: Canvas) {
    if (!this.raw) return;
    const {raw} = this;
    const {width, height} = raw;
    const {tile} = this.options;
    const ctx = canvas.getContext('2d');
    getTilesGroups(width, height, tile).forEach((tiles) => {
      const shuffleInd = generateShuffleIndex(tiles.length, this.options.seed);
      const group = getTilesGroupInfo(tiles);
      return tiles.forEach((t, i) => {
        const {w, h} = t.size;
        const pos = getPartPos(shuffleInd[i], group.width, tile);
        const srcPos = getPartPos(i, group.width, tile);
        ctx.drawImage(
          raw,
          group.x + srcPos.x,
          group.y + srcPos.y,
          w,
          h,
          group.x + pos.x,
          group.y + pos.y,
          w,
          h,
        );
      });
    });
  }

  async buffer(): Promise<Buffer | null> {
    if (!this.raw) return null;
    const {raw} = this;
    const {width, height} = raw;
    trackMemory('export');
    const canvas = createCanvas(width, height);
    trackMemory('export');
    await this.shake(canvas);
    trackMemory('export');
    return canvas.toBuffer();
  }

  async export(filepath: string) {
    const buff = await this.buffer();
    if (buff) {
      writeFileSync(filepath, buff);
    }
  }
}
