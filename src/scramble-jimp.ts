import Jimp from 'jimp';
import axios from 'axios';
import {ScrambleOptions} from '.';
import {getTilesGroupInfo, getTilesGroups} from './group';
import {getPartPos} from './part';
import {generateShuffleIndex} from './shuffle';
import {trackMemory} from './track';

export class Scrambler {
  constructor(private options: ScrambleOptions) {}

  private raw?: Jimp;
  private width = 0;
  private height = 0;

  async loadUrl(url: string) {
    const {data} = await axios(url, {responseType: 'arraybuffer'});
    this.raw = await Jimp.read(Buffer.from(data, 'binary'));
    this.width = this.raw?.bitmap.width;
    this.height = this.raw?.bitmap.height;
  }

  private async shake(img: Jimp) {
    if (!this.raw) return;
    const {raw, width, height} = this;
    const {tile} = this.options;
    getTilesGroups(width, height, tile).forEach((tiles) => {
      const shuffleInd = generateShuffleIndex(tiles.length, this.options.seed);
      const group = getTilesGroupInfo(tiles);
      tiles.forEach((t, i) => {
        const {w, h} = t.size;
        const pos = getPartPos(shuffleInd[i], group.width, tile);
        const srcPos = getPartPos(i, group.width, tile);
        img.blit(
          raw,
          group.x + pos.x,
          group.y + pos.y,
          group.x + srcPos.x,
          group.y + srcPos.y,
          w,
          h,
        );
      });
    });
  }

  async export(filepath: string) {
    if (!this.raw) return;
    trackMemory('export');
    const img = await Jimp.create(this.raw);
    trackMemory('export');
    await this.shake(img);
    trackMemory('export');
    await img.writeAsync(filepath);
    trackMemory('export');
  }

  async buffer(mimeType: string): Promise<Buffer | null> {
    if (!this.raw) return null;
    const img = await Jimp.create(this.raw);
    await this.shake(img);
    return await img.getBufferAsync(mimeType);
  }
}
